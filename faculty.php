<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>
    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Faculty</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Faculty</span></a>
						</li>
						<li class="trail-item trail-end"><span>Faculty</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="pt-50 pb-50 gray-bg">
		<div class="container">
			<div class="section-title pb-45">
				<h2>Our Top Instructors</h2>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty6.png" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Karthikeyan</h6>
								<span>Endodontics</span>
							</div>
						</a>
					</div>
                </div>
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty1.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Anish Poorna</h6>
								<span>Oral & Maxillofacial Surgery</span>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty2.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Joshna EK</h6>
								<span>Oral & Maxillofacial Surgery</span>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty3.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Kavitha Shankar</h6>
								<span>Oral & Maxillofacial Pathology</span>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty4.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Venkata Krishnan</h6>
								<span>Orthodontics</span>
							</div>
						</a>
					</div>
				</div>
                <div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty5.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Pavithra Devi .K</h6>
								<span>Pedodontics</span>
							</div>
						</a>
					</div>
				</div>
                <div class="col-lg-3 col-md-6 col-sm-8">
                </div>
                <div class="col-lg-3 col-md-6 col-sm-8">
                </div>
			</div>
		</div>
	</section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>