<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Blog Details</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Blog</span></a>
						</li>
						<li class="trail-item trail-end"><span>Blog</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="event-area pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<img src="../images/blog/neet-preparation.jpeg" class="mt-3" alt="">
					<div class="overview">
						<div class="section-title pb-10">
							<h5>Strategies in Dental Anatomy, Embryology and Oral Histology Preparation</h5>
                            <div class="pt-3">
                                <em style="font-weight:500">"Try to learn something about everything and everything about something!"</em>
                                <em style="font-weight:700;color:#FF6200;font-size:14px"> - Thomas Henry Huxley </em>
                            </div>
                            <p class="content">Dental Anatomy and Oral Histology, being the very basic and the core for dentists, is one subject you should begin with. If not for us, then who will learn everything about teeth. Also, if not this, what else will we learn everything about.</p>
                            <p><span>Recommended book for Dental Anatomy:</span></p>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img src="./images/blog/blog-dental-anatology.jpg" alt="Blog Image">
                                </div>
                            </div>
                            <p><span>Recommended books for Oral Histology:</span></p>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img src="./images/blog/blog-oral-histology.jpg" alt="Blog Image">
                                </div>
                                <div class="col-lg-3">
                                    <img src="./images/blog/blog-oral-histology1.jpg" alt="Blog Image">
                                </div>
                            </div>
                            <p class="content">Wheeler’s is the gold standard for Dental Anatomy. For Oral Histology, you can refer to either of these two: Orban’s or Ten Cate’s, whichever you had used in your first year is more preferred.</p>
                            <p><span>Important chapters:</span></p>
                            <ul class="b">
                                <li><i class="fa fa-circle"></i>Primary and permanent dentition</li>
                                <li><i class="fa fa-circle"></i>Eruption and shedding of teeth</li>
                                <li><i class="fa fa-circle"></i>Occlusion</li>
                                <li><i class="fa fa-circle"></i>Physiologic form of teeth</li>
                                <li><i class="fa fa-circle"></i>Development and Growth of teeth</li>
                                <li><i class="fa fa-circle"></i>Enamel</li>
                                <li><i class="fa fa-circle"></i>Dentin</li>
                                <li><i class="fa fa-circle"></i>Cementum</li>
                                <li><i class="fa fa-circle"></i>Pulp</li>
                                <li><i class="fa fa-circle"></i>Periodontal ligament</li>
                                <li><i class="fa fa-circle"></i>Bone</li>
                                <li><i class="fa fa-circle"></i>Salivary glands</li>
                                <li><i class="fa fa-circle"></i>Maxillary sinus</li>
                                <li><i class="fa fa-circle"></i>Oral mucous membrane</li>
                            </ul>
                            <p><span>Method of preparation:</span></p>
                            <p class="content">As per NEET-MDS subject-wise question distribution, 14 questions will be asked from Oral Anatomy and Histology.When you start your preparation, you should get atleast one quick reading of the standard textbooks. As the recent trend has been more of image based and conceptual questions, skimming through your textbooks, will develop your picture memory, and not to forget, a strong basic foundation, will help to apply the concepts to solve concept-based questions.</p>
                            <p class="content">You can also listen to Master class lectures (videos) for quick go through. Start solving MCQs chapterwise either from preparatory books or from the question bank in mobile apps. Revise, again and again, go back to your books for concepts you have forgotten.</p>
                            <p class="content">When you feel exhausted of reading, simply turning the pages of textbooks and looking at the images, will both save time and enhance memory. Never judge by yourselves, as to content which are simple and easy or difficult ones. For you never know the mindset of question paper setter. Questions on identification of tooth based on occlusal morphology, as well as questions on genetics are also asked. Prepare yourself for the diverse questions by attempting mock test series.</p>
                            <p class="content">Oral Anatomy and Histology is a subject, where you should not omit anything; once you are good with the normal, you can easily learn the abnormal (pathology). Oral Pathology forms the basis for all the final year subjects, which in turn is dependent on Oral Histology for basics. If you keep accumulating stuff or try to crack it yourself, you may spend too much time over it. Reach out to experts for guidance and doubt solving as and when required.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="course-features">
						<div class="course-info">
							<h2 class="widget-title">Related Posts</h2>
                            <ul class="course-info-list">
                                <li><i class="fa fa-angle-right"></i><a href="./blog-az-tactics-in-mds-preparation.php"><span class="label">A to Z tactics in MDS preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-what-neet-mds-demands-you-to-know.php"><span class="label">What NEET MDS demands you to know?</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-pathology-preparation.php"><span class="label">Strategies in Oral Pathology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-conservative-dentistry-and-endodontics-preparation.php"><span class="label">Strategies in Conservative Dentistry and Endodontics Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-radiology-preparation.php"><span class="label">Strategies in Oral Radiology Preparation</span></a></li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>