<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>
    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">About Us</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>About Us</span></a>
						</li>
						<li class="trail-item trail-end"><span>About Us</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="infra-area pt-50 pb-50">
		<div class="container">
			<div class="row align-items-center pb-45">
				<div class="col-lg-5 col-md-6">
					<div class="welcome-thumb">
						<img src="./images/gallery/15.jpg" alt="welcome-image">
					</div>
				</div>
				<div class="col-lg-7 col-md-6 section-left-content">
					<span class="section-left-head-top">Welcome to </span>
					<h2 class="section-left-head">Ardent MDS</h2>
					<p>Ardent MDS is an enthusiastic dental platform for enlightening the future of dental students through various competitive exams. It has both offline and online centers located in 3 different places Chennai, Melmaruvathur and Nagercoil. Ardent MDS is our official app available in Play store. We are focused to give quality education to every dental student in India to understand the concepts in a simple, better understandable way.</p>
                    <p>The institute's aim is not only to provide specific knowledge and strengthen the foundation of the students in PG Dental Entrance, but also to infuse them with determination to crack the Entrance Exams with flying colours. To explore the potential of the students and to help them master the subject, we, at Ardent MDS have developed extensive scientific teaching as well as testing methods.</p>
				</div>
			</div>
            <div class="section-title infra-gallery pb-45">
				<h5>Gallery</h5>
				<div class="row campus-gallery mt-30">
					<div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/01.jpg" title="Class Room"><img src="./images/gallery/01.jpg" alt=""></a>
					</div>
					<div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/02.jpg" title="Class Room"><img src="./images/gallery/02.jpg" alt=""></a>
					</div>
					<div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/04.jpg" title="Class Room"><img src="./images/gallery/04.jpg" alt=""></a>
					</div>
					<div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/05.jpg" title="Class Room"><img src="./images/gallery/05.jpg" alt=""></a>
					</div>
                    <div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/06.jpg" title="Class Room"><img src="./images/gallery/06.jpg" alt=""></a>
					</div>
                    <div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/07.jpg" title="Class Room"><img src="./images/gallery/07.jpg" alt=""></a>
					</div>
                    <div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/08.jpg" title="Class Room"><img src="./images/gallery/08.jpg" alt=""></a>
					</div>
                    <div class="col-lg-4 img-hover-zoom--brightness">
						<a href="./images/gallery/09.jpg" title="Class Room"><img src="./images/gallery/09.jpg" alt=""></a>
					</div>
				</div>
			</div>
            <div class="row align-items-center">
				<div class="col-lg-7 col-md-6 section-left-content">
					<span class="section-left-sub-title">What is Our Strategy to Success?</span>
					<p>We are committed to provide each student the most distinguished coaching platform for uninterrupted learning and intelligent testing approach. All of the following features are an integrated part of the Ardent MDS system.</p>
                    <ul class="custom">
                    	<li>Follow Best Practices</li>
                        <li>Content rich Systematic Teaching</li>
                        <li>Exam Strategy &amp; Mentoring Sessions</li>
                        <li>Intelligent Test Structure</li>
                        <li>Experienced &amp; Renowned Faculty</li>
                    </ul>
				</div>
				<div class="col-lg-5 col-md-6">
					<div class="welcome-thumb">
						<img src="./images/gallery/12.jpg" alt="welcome-image">
					</div>
				</div>
			</div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="./js/plugins.js" type="text/javascript"></script>
<script src="./js/active.js" type="text/javascript"></script>
<script src="./js/main.js" type="text/javascript"></script>
<script src="./js/slick.min.js" type="text/javascript"></script>
<script src="./owl-carousel/js/owl.carousel.min.js"></script>
<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>