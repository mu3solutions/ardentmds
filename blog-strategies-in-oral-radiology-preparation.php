<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Blog Details</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Blog</span></a>
						</li>
						<li class="trail-item trail-end"><span>Blog</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="event-area pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<img src="../images/blog/neet-preparation.jpeg" class="mt-3" alt="">
					<div class="overview">
						<div class="section-title pb-10">
							<h5>Strategies in Oral Radiology Preparation</h5>
                            <div class="pt-3">
                                <em style="font-weight:500">"Remove radiology from Medical science, and vast majority of cases can become unreasonably difficult to be solved!"</em>
                                <em style="font-weight:700;color:#FF6200;font-size:14px"> - Haider Idam</em>
                            </div>
                            <p class="content">Medical science is inseparable from Radiology. Diagnosis cannot be finalized unless supported with Radiographic/Histopathological diagnosis. Dentistry is no exception.</p>
                            <p><span>Recommended books for Oral Radiology:</span></p>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img src="./images/blog/blog-oral-radiology.png" alt="Blog Image">
                                </div>
                                <div class="col-lg-3">
                                    <img src="./images/blog/blog-oral-radiology1.jpg" alt="Blog Image">
                                </div>
                            </div>
                            <p><span>Important chapters:</span></p>
                            <ul class="b">
                                <li><i class="fa fa-circle"></i>Radiation Physics</li>
                                <li><i class="fa fa-circle"></i>Radiation Biology, Safety and Protection</li>
                                <li><i class="fa fa-circle"></i>Imaging and Processing</li>
                                <li><i class="fa fa-circle"></i>Projection Geometry</li>
                                <li><i class="fa fa-circle"></i>Intraoral Radiography</li>
                                <li><i class="fa fa-circle"></i>Extraoral Radiography</li>
                                <li><i class="fa fa-circle"></i>Specialized Radiographic Techniques</li>
                                <li><i class="fa fa-circle"></i>Radiographic Diagnosis</li>
                            </ul>
                            <p><span>Method of preparation:</span></p>
                            <p class="content">Oral Medicine and Radiology together account to about 14 questions in the subject wise distribution of NEET-MDS. Not only does Radiology come with Oral Medicine, it also comes along with questions under Oral Surgery, Orthodontics, Endodontics, Pedodontics and Periodontics. As a holistic approach to any subject for that matter, we need to know, right from the Fundamentals of radiation, properties of x-rays, the X-ray machine, film and its accessories, the various laws governing the production of X-rays, the effects of radiation, radiosensitivity and radioresistivity, methods of protection from radiation. A day without an Intraoral radiograph is considered a bad day at a clinic.</p>
                            <p class="content">The different techniques, types of films used, projection of x-rays and processing of films are inevitable; unless you know the normal, you cannot differentiate the abnormal (pathology) and the errors – so learning the normal anatomical landmarks, and interpretation of radiograph is important, not only for your exam but your clinical practice as well. Extraoral radiographic techniques, (OPG in particular) and specialized techniques such as Xeroradiography, Computed Tomography, CBCT, PET, SPECT, Ultrasonography, Nuclear medicine, Magnetic Resonance Imaging, TMJ imaging – Arthrography and arthroscopy, Salivary gland imaging – Sialography and Scintigraphy are all desirable to know and fall under moderate to difficult category of questions.</p>
                            <p class="content">Radiology not only helps in diagnosis, but also in treatment. Radiotherapy, various types, protocols, consequences and management of radiotherapy patients, is yet again an area of interest. The various radiographic features and appearances – their resemblance and eponyms; numerical, facts, concepts are all to be revised multiple times Your textbooks are of great help for images. Observe minutely the similarities and differences in each of the radiographs. Give importance to the description, as in site, size, and clinical features of the lesion and correlate with the radiographs, to arrive at diagnosis.</p>
                            <p class="content">Solving MCQs chapter wise either from preparatory books or from the question bank in mobile apps, is a great way of preparation Learn and re-learn. Reach out to experts for guidance and doubt solving as and when required.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="course-features">
						<div class="course-info">
							<h2 class="widget-title">Related Posts</h2>
                            <ul class="course-info-list">
                                <li><i class="fa fa-angle-right"></i><a href="./blog-az-tactics-in-mds-preparation.php"><span class="label">A to Z tactics in MDS preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-what-neet-mds-demands-you-to-know.php"><span class="label">What NEET MDS demands you to know?</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-pathology-preparation.php"><span class="label">Strategies in Oral Pathology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-dental-anatomy-embryology-and-oral-histology-preparation.php"><span class="label">Strategies in Dental Anatomy, Embryology and Oral Histology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-conservative-dentistry-and-endodontics-preparation.php"><span class="label">Strategies in Conservative Dentistry and Endodontics Preparation</span></a></li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>