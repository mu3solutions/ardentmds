<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <div class="owl-carousel slide-one-item">
		<div class="site-section-cover img-bg-section" style="background-image: url('images/slider/slider1.jpg'); "></div>
		<div class="site-section-cover img-bg-section" style="background-image: url('images/slider/slider2.png'); "></div>
		<div class="site-section-cover img-bg-section" style="background-image: url('images/slider/slider3.jpg'); "></div>	
	</div>

    <section class="gray-bg">
		<div class="container mt-30 pb-25">
			<div class="marquee">
				<div class="notice">
					<h6>Important</h6>
				</div>
				<marquee class="GeneratedMarquee" direction="left" scrollamount="5" behavior="scroll" onmouseover="this.stop();" onmouseout="this.start();">Basic registration started for INICET Exam, June session. For further details, contact <a href="https://www.aiimsexams.ac.in/">www.aiimsexams.org.</a></marquee>
			</div>
		</div>
	</section>

	<section class="class pb-50 pt-50 gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6"><a href="./app-landing/index.php"><img src="./images/background/online-class.png" alt="Online Class"></a></div>
				<div class="col-lg-6 col-md-6"><a href="./offline.php"><img src="./images/background/offline-classes.png" alt="Offine Class"></a></div>
			</div>
		</div>
	</section>

    <section class="feature-area text-center">
		<div class="container">
			<div class="row feature-content-wrap">
				<div class="col-lg-4 col-md-4">
					<div class="feature-item feature-item1">
						<div class="hover-overlay"></div>
						<i class="fas fa-video feature__icon"></i>
						<h3 class="feature__title">Learn</h3>
						<p class="feature__text" style="min-height: 112px">Comprehensive videos and live lectures for every chapter to develop solid foundation for attempting questions.</p>
						<a href="#" style="background:#2475B0;" class="feature__btn ">200+ Videos</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="feature-item feature-item2">
						<div class="hover-overlay"></div>
						<i class="fas fa-book feature__icon"></i>
						<h3 class="feature__title">Practice</h3>
						<p class="feature__text" style="min-height: 112px">Question bank with explanation and doubt support for each question to build rigour and conceptual clarity.</p>
						<a href="#" style="background: #3498DB;" class="feature__btn">15000+ MCQs</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="feature-item feature-item3">
						<div class="hover-overlay"></div>
						<i class="far fa-chart-bar feature__icon"></i>
						<h3 class="feature__title">Analyze</h3>
						<p class="feature__text" style="min-height: 112px">Give tests for chapter, target, module and complete syllabus to assess current preparation level &amp; know steps to reach next level.</p>
						<a href="#" style="background:#2475B0;" class="feature__btn btn text-white">250+ Tests</a>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="chose-area pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="section-title pb-4">
						<h5>Get started with us</h5>
						<h2>Privilege of joining us</h2>
					</div>
					<p>Ardent MDS is a enthusiastic fastest growing digital dental coaching platform with strong core team of faculties,  Question bank with recent trend, updated test series, and many innovative features like 3 minutes challenge, 10 minutes concept, clinical case discussion, and motivational corner.Join with us, became a part of our Ardent MDS Family. Increase your chance of success into 3X.</p>
				</div>
				<div class="col-lg-6">
					<div class="row benefit-course-box mt-4">
                        <div class="col-lg-4 col-sm-6">
                            <div class="benefit-item benefit-item1">
                                <i class="fas fa-book-open benefit__icon"></i>
                                <h4 class="benefit__title">30+ courses</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="benefit-item benefit-item2">
                                <i class="fas fa-chalkboard benefit__icon"></i>
                                <h4 class="benefit__title">Live Sessions</h4>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="benefit-item benefit-item3">
                                <i class="fas fa-users benefit__icon"></i>
                                <h4 class="benefit__title">Expert Teachers</h4>
                            </div>
                        </div>
						<div class="col-lg-4 col-sm-6">
                            <div class="benefit-item benefit-item1">
								<i class="fas fa-bell benefit__icon"></i>
                                <h4 class="benefit__title">Test Series</h4>
                            </div>
                        </div>
						<div class="col-lg-4 col-sm-6">
                            <div class="benefit-item benefit-item2">
								<i class="fas fa-eye benefit__icon"></i>
                                <h4 class="benefit__title">Doubt Clearing</h4>
                            </div>
                        </div>
						<div class="col-lg-4 col-sm-6">
                            <div class="benefit-item benefit-item3">
								<i class="fas fa-video benefit__icon"></i>
                                <h4 class="benefit__title">Video Lectures</h4>
                            </div>
                        </div>
                    </div>
				</div>
			</div> 
		</div>
	</section>

    <section class="facilities pb-50 pt-50 gray-bg">
		<div class="container">
			<div class="section-title pb-45 text-center">
				<h2>Our Facilities</h2>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="service">
						<i class="fas fa-users"></i>
						<h3>Experienced Teachers</h3>
						<p>Ardent MDS has a team of dedicated teachers, experienced in the field of dental education, preparing students for NEET MDS, striving to give the best in simple yet profound manner, since several years.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="service">
						<i class="fa fa-building"></i>
						<h3>Accessible location</h3>
						<p>Ardent MDS has its offline centre at Guindy, Chennai, near the heart of the city, easily accessible to various modes of transportation such as Chennai metro, MTC bus stops and Railway station.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="service">
						<i class="fa fa-cogs"></i>
						<h3>Innovative Methodology</h3>
						<p>Ardent MDS has a superior competence with innovative learning approach from traditional to recent concepts; the offline classes are integrated with online app for comprehensive understanding.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="service">
						<i class="fa fa-book"></i>
						<h3>Doubt clearing session</h3>
						<p>Students can interact with faculties after every class and clarify their doubts promptly. Ardent MDS gives personal guidance and motivation to their students, which is one of our unique features.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="service">
						<i class="fa fa-line-chart"></i>
						<h3>Regular Test & Analysis </h3>
						<p>Students can evaluate their progress in NEET MDS preparation, by attending regular tests in various aspects. Every test is scrutinized and a complete analysis is given, to help the students evaluate their strength and weakness and modify their preparation periodically.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="service">
						<i class="fa fa-address-card-o"></i>
						<h3>Track Record</h3>
						<p>A glimpse of the track record provides an idea of the quality of education we provide; our efficiency in getting students prepared for the exams is evident from our students being alumni or pursuing postgraduation in India’s prestigious institutions.</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="news-part" class="pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="section-title pb-50">
						<h5>Latest News</h5>
						<h2>From the Ardent MDS</h2>
					</div>
				</div>
			</div> <!-- row -->
			<div class="row">
				<div class="col-lg-6">
					<div class="single-news mt-30">
						<div class="news-thum pb-25">
							<img src="./images/blog/blog-a-z-1.jpg" alt="News">
						</div>
						<div class="news-cont">
							<ul>
								<li><a href="#"><i class="fa fa-calendar"></i>31 Jan 2021 </a></li>
								<li><a href="#"> <span>By</span> Dr. Karthikeyan</a></li>
							</ul>
							<a href="./blog-az-tactics-in-mds-preparation.php"><h3>A to Z tactics in MDS preparation</h3></a>
							<p>Never mind whatever you are doing, may it be undergraduation, internship or post-internship, start now! There’s never a wrong time, to do the right thing. All you need is an aspiring mind and a willing heart to achieve your dreams.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="single-news news-list">
						<div class="row">
							<div class="col-sm-4">
								<div class="news-thum mt-30">
									<img src="./images/blog/neet-preparation-1.jpg" alt="News">
								</div>
							</div>
							<div class="col-sm-8">
								<div class="news-cont mt-30">
									<ul>
										<li><a href="#"><i class="fa fa-calendar"></i>4 Feb 2021 </a></li>
										<li><a href="#"> <span>By</span> Dr. Karthikeyan</a></li>
									</ul>
									<a href="./blog-what-neet-mds-demands-you-to-know.php"><h3>What NEET MDS demands you to know?</h3></a>
									<p class="text-trun">NEET stands for National Eligibility – cum – Entrance Test. It tests your eligibility to enter MDS (Master of Dental Surgery) through the knowledge gained during your BDS (Bachelor of Dental Surgery) course.</p>
								</div>
							</div>
						</div>
					</div> 
					<div class="single-news news-list">
						<div class="row">
							<div class="col-sm-4">
								<div class="news-thum mt-30">
									<img src="./images/blog/neet-preparation-1.jpg" alt="News">
								</div>
							</div>
							<div class="col-sm-8">
								<div class="news-cont mt-30">
									<ul>
										<li><a href="#"><i class="fa fa-calendar"></i>10 Feb 2021 </a></li>
										<li><a href="#"> <span>By</span> Dr. Karthikeyan</a></li>
									</ul>
									<a href="./blog-strategies-in-oral-pathology-preparation.php"><h3>Strategies in Oral Pathology Preparation</h3></a>
									<p class="text-trun">Oral Pathology forms the basis for all the final year subjects, which in turn is derives its basics from Oral Histology.</p>
								</div>
							</div>
						</div>
					</div> 
					<div class="single-news news-list">
						<div class="row">
							<div class="col-sm-4">
								<div class="news-thum mt-30">
									<img src="./images/blog/neet-preparation-1.jpg" alt="News">
								</div>
							</div>
							<div class="col-sm-8">
								<div class="news-cont mt-30">
									<ul>
										<li><a href="#"><i class="fa fa-calendar"></i>17 Feb 2020 </a></li>
										<li><a href="#"> <span>By</span> Adam linn</a></li>
									</ul>
									<a href="./blog-strategies-in-dental-anatomy-embryology-and-oral-histology-preparation.php" class="text-trun"><h3>Strategies in Dental Anatomy, Embryology and Oral Histology Preparation</h3></a>
									<p class="text-trun">Dental Anatomy and Oral Histology, being the very basic and the core for dentists, is one subject you should begin with. If not for us, then who will learn everything about teeth. Also, if not this, what else will we learn everything about.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="pt-50 pb-50 gray-bg">
		<div class="container">
			<div class="section-title pb-45">
				<h5>Meet our Faculty</h5>
				<h2>Our Top Instructors</h2>
			</div>
			<div class="row justify-content-center">
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty1.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Anish Poorna</h6>
								<span>Oral & Maxillofacial Surgery</span>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty2.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Jashna EK</h6>
								<span>Oral & Maxillofacial Surgery</span>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty3.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Kavitha Shankar</h6>
								<span>Oral & Maxillofacial Pathology</span>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-8">
					<div class="single-teacher-2 text-center mt-30">
						<div class="teacher-image">
							<img src="./images/faculty/faculty4.jpg" alt="Teacher">
						</div>
						<a href="#">
							<div class="teacher-content">
								<h6 class="teacher-title">Dr. Venkata Krishnan</h6>
								<span>Orthodontics</span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="app-area pt-50 pb-45">
		<div class="container">
			<div class="row">
				<div class="image-column col-lg-6 col-md-12 col-sm-12">
					<div class="image">
						<img src="./images/background/app-screen.jpg" alt="">
					</div>
				</div>
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column pt-40">
						<div class="section-title">
							<h5>Check your Progress</h5>
							<h2>Digitial Learning App</h2>
						</div>
						<p class="mt-3">Ardent MDS provides Quality education for pg aspirants preparing for competitive Exams. Ardent MDS, India’s one of the rapid emerging learning platform is your companion throughout your journey of exam preparation. In Ardent app, you can attend Live classes, Recorded classes by top faculties, get your doubts cleared and evaluate your preparation through various test series & Practice session.</p>
                        <a href="./app-landing/index.php" target="_blank" class="btn btn-theme btn-md effect">Know More</a>
						<div class="app-btns mt-4">
							<a href="https://play.google.com/store/apps/details?id=com.ardentmds" target="_blank"><img src="./images/background/app-2.png" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    
			
	

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>