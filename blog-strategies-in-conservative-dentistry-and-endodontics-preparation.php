<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Blog Details</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Blog</span></a>
						</li>
						<li class="trail-item trail-end"><span>Blog</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="event-area pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<img src="../images/blog/neet-preparation.jpeg" class="mt-3" alt="">
					<div class="overview">
						<div class="section-title pb-10">
							<h5>Strategies in Conservative Dentistry and Endodontics Preparation</h5>
                            <div class="pt-3">
                                <em style="font-weight:500">"Perfection is not attainable, but if we chase perfection we can catch excellence."</em>
                                <em style="font-weight:700;color:#FF6200;font-size:14px"> - Vince Lombardi</em>
                            </div>
                            <p><span>Recommended book for Endodontics:</span></p>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img src="./images/blog/blog-endodo.jpg" alt="Blog Image">
                                </div>
                            </div>
                            <p><span>Important chapters:</span></p>
                            <ul class="b">
                                <li><i class="fa fa-circle"></i>Anatomy of the pulp cavity</li>
                                <li><i class="fa fa-circle"></i>Diseases of the pulp and periradicular tissues</li>
                                <li><i class="fa fa-circle"></i>Cleaning and shaping of root canals</li>
                                <li><i class="fa fa-circle"></i>Irrigation and Intracanal medicaments</li>
                                <li><i class="fa fa-circle"></i>Obturation</li>
                                <li><i class="fa fa-circle"></i>Traumatology</li>
                                <li><i class="fa fa-circle"></i>Endodontic emergencies</li>
                                <li><i class="fa fa-circle"></i>Endodontic surgery</li>
                                <li><i class="fa fa-circle"></i>Bleaching</li>
                            </ul>
                            <p><span>Recommended book for Operative Dentistry:</span></p>
                            <div class="row">
                                <div class="col-lg-3">
                                    <img src="./images/blog/blog-density.jpg" alt="Blog Image">
                                </div>
                            </div>
                            <p><span>Important chapters:</span></p>
                            <ul class="b">
                                <li><i class="fa fa-circle"></i>Instrumentation</li>
                                <li><i class="fa fa-circle"></i>Fundamentals of tooth preparation</li>
                                <li><i class="fa fa-circle"></i>Dental cements</li>
                                <li><i class="fa fa-circle"></i>Amalgam</li>
                                <li><i class="fa fa-circle"></i>Direct filling gold</li>
                                <li><i class="fa fa-circle"></i>Cast restoration</li>
                                <li><i class="fa fa-circle"></i>Composites</li>
                                <li><i class="fa fa-circle"></i>Restoring contacts and contours</li>
                                <li><i class="fa fa-circle"></i>Non carious lesions and management</li>
                                <li><i class="fa fa-circle"></i>Dentin Hypersensitivity</li>
                            </ul>
                            <p><span>Method of preparation:</span></p>
                            <p class="content">As per NEET-MDS subject-wise question distribution, 14 questions will be asked from Conservative Dentistry and Endodontics. Dentistry as a whole is an ever-growing subject. Conservative Dentistry and Endodontics in particular, has innovations and advancements, in diagnostic techniques, treatment approach, instruments and materials. To gain the knowledge about endodontic treatment on the basis of scientific foundation, and to possess the skills to use and familiarise endodontic instruments and materials is an important objective in learning endodontics.</p>
                            <p class="content">“Of all the phases of anatomic study in the human system, one of the most complex is the pulp cavity morphology” – Although numerous morphological variations occur, these changes usually follow a general pattern, and hence this study is an important undertaking. Diagnostic techniques from the traditional to latest methods, Instruments – classifications, uses and modifications, Working length determination – various methods and concepts, Cleaning and shaping objectives and techniques are most important topics to be covered in Endodontics. The principles and fundamentals of cavity preparation, is the first step towards perfection in Operative Dentistry.</p>
                            <p class="content">Knowledge of the restorative materials, from its inventions and the modifications that followed until the latest trends are must to know topics. To help with the image based questions, skimming through your textbooks, is of immense help to aid picture memory. Solving MCQs chapter wise either from preparatory books or from the question bank in mobile apps, is an ideal way of preparation. Revise, again and again. Reach out to experts for guidance and doubt solving as and when required.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="course-features">
						<div class="course-info">
							<h2 class="widget-title">Related Posts</h2>
                            <ul class="course-info-list">
                                <li><i class="fa fa-angle-right"></i><a href="./blog-az-tactics-in-mds-preparation.php"><span class="label">A to Z tactics in MDS preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-what-neet-mds-demands-you-to-know.php"><span class="label">What NEET MDS demands you to know?</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-pathology-preparation.php"><span class="label">Strategies in Oral Pathology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-dental-anatomy-embryology-and-oral-histology-preparation.php"><span class="label">Strategies in Dental Anatomy, Embryology and Oral Histology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-radiology-preparation.php"><span class="label">Strategies in Oral Radiology Preparation</span></a></li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>