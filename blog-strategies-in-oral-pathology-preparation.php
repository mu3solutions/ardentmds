<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Blog Details</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Blog</span></a>
						</li>
						<li class="trail-item trail-end"><span>Blog</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="event-area pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<img src="./images/blog/neet-preparation.jpeg" class="mt-3" alt="">
					<div class="overview">
						<div class="section-title pb-10">
							<h5>Strategies in Oral Pathology Preparation</h5>
                            <div class="pt-3">
                                <em style="font-weight:500">"You can’t build a great building on a weak foundation; You must have a solid foundation, if you are going to have a strong superstructure."</em>
                                <em style="font-weight:700;color:#FF6200;font-size:14px"> - Gordon B Hinckley </em>
                            </div>
                            <p class="content">Oral Pathology forms the basis for all the final year subjects, which in turn is derives its basics from Oral Histology.</p>
                            <p><span>Recommended books for Oral Pathology:</span></p>
                            <div class="row">
                                <div class="col-lg-3"><img src="./images/blog/blog-oral-pathology.png" alt="Blog Image"></div>
                                <div class="col-lg-3"><img src="./images/blog/blog-oral-pathology1.jpg" alt="Blog Image"></div>
                            </div>
                            <p><span>Important chapters:</span></p>
                            <ul class="b">
                                <li><i class="fa fa-circle"></i>Developmental disturbances</li>
                                <li><i class="fa fa-circle"></i>Odontogenic cysts and tumours</li>
                                <li><i class="fa fa-circle"></i>Benign and Malignant tumours of oral cavity</li>
                                <li><i class="fa fa-circle"></i>Diseases of Salivary glands</li>
                                <li><i class="fa fa-circle"></i>Bacterial Infections</li>
                                <li><i class="fa fa-circle"></i>Viral infections</li>
                                <li><i class="fa fa-circle"></i>Mycotic infections</li>
                                <li><i class="fa fa-circle"></i>Dental caries</li>
                                <li><i class="fa fa-circle"></i>Diseases of pulp and periapical tissues</li>
                                <li><i class="fa fa-circle"></i>Periodontal diseases</li>
                                <li><i class="fa fa-circle"></i>Bone and Joints diseases</li>
                                <li><i class="fa fa-circle"></i>Haematologic diseases</li>
                                <li><i class="fa fa-circle"></i>Diseases of the skin</li>
                                <li><i class="fa fa-circle"></i>Diseases of Nerves and Muscles</li>
                                <li><i class="fa fa-circle"></i>Oral aspects of Metabolic diseases</li>
                                <li><i class="fa fa-circle"></i>Physical and Chemical Injuries of oral cavity</li>
                                <li><i class="fa fa-circle"></i>Allergic and Immunologic diseases of the oral cavity</li>
                                <li><i class="fa fa-circle"></i>Forensic Odontology</li>
                            </ul>
                            <p><span>Method of preparation:</span></p>
                            <p class="content">As per NEET-MDS subject-wise question distribution, 14 questions will be asked from Oral Pathology and Oral Microbiology.Creating a very strong foundation with Oral Pathology, will make it easy for almost all subjects of the final year. Once you are well versed with Etiology, Pathogenesis, Clinical features and Histopathology of the varied diseases of the oral cavity, you can apply this knowledge for treatment options in Oral Medicine, surgical aspects in Oral Surgery, and other subjects as well.</p>
                            <p class="content">Allot a major part of your preparation time for Oral Pathology. You have a wide range of <b>syndromes</b> to be learnt – compare the similarities and differences, group them under various categories and memorise the multiple names of those syndromes; this way you will remember the important features of each of the syndromes. There are many signs and symptoms with eponyms (<b>Eponym:</b> Something named after someone), which are commonly asked questions. Eg: Nikolsky’s sign, Tzanck cell, Tzanck test, Russell bodies, etc. Also not to forget the diseases and syndromes with eponyms.</p>
                            <p class="content">Similar names for different diseases and features, Different diseases with similar features, peculiar characteristics of various diseases, are few things that have to grouped and studied. In recent times, importance has been given to clinical questions and image-based questions, for which Oral Pathology is the apt subject. The common site of diseases, commonly involved teeth, age and gender of frequent occurrence, important clinical and histological features are the hints to be applied for clinical questions.</p>
                            <p class="content">Effects of vitamin and hormonal imbalances, anemia and likely ones, fall under varied subjects like, Physiology, Biochemistry, General Pathology and General Medicine. A better understanding and reading of such topics during preparation of one subject, saves time with the other subjects. As Oral Pathology and Oral Microbiology is one subject on the whole, causative organisms of diseases, is yet another area of interest for framing questions. Forensic Odontology being a major by itself, will take over 2 to 3 questions. </p>
                            <p class="content">To help with the image based questions, skimming through your textbooks, is of immense help to aid picture memory. Solving MCQs chapter wise either from preparatory books or from the question bank in mobile apps, is a great way of preparation. Revise, again and again. Reach out to experts for guidance and doubt solving as and when required.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="course-features">
						<div class="course-info">
							<h2 class="widget-title">Related Posts</h2>
                            <ul class="course-info-list">
                                <li><i class="fa fa-angle-right"></i><a href="./blog-az-tactics-in-mds-preparation.php"><span class="label">A to Z tactics in MDS preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-what-neet-mds-demands-you-to-know.php"><span class="label">What NEET MDS demands you to know?</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-dental-anatomy-embryology-and-oral-histology-preparation.php"><span class="label">Strategies in Dental Anatomy, Embryology and Oral Histology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-conservative-dentistry-and-endodontics-preparation.php"><span class="label">Strategies in Conservative Dentistry and Endodontics Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-radiology-preparation.php"><span class="label">Strategies in Oral Radiology Preparation</span></a></li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>