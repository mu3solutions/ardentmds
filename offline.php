<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>
    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Offline Class</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Courses</span></a>
						</li>
						<li class="trail-item trail-end"><span>Courses</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="infra-area pt-50 pb-50">
		<div class="container">
            <span class="section-left-sub-title text-center">Be smart & take your future in Your Hand</span>
            <p class="text-center">With the launch of Ardent MDS app a large number of aspirants, who otherwise couldn't take the advantage of the Ardent teaching because of various reasons like non-availability of Ardent MDS centre in the vicinity would be greatly benefited. This is the premier course offered by Ardent MDS, with detailed classes on both medical and dental subjects in Ardent MDS app. Our main asset for this course is our faculty which is a mix of young MDS toppers and experienced faculty members who will mentor you to success in MDS entrance examination.</p>
            <div class="row mt-5">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="feature-box text-center">
                        <div class="feature-header">
                            <div class="feature-icon">
                                <span class="circle"></span><i class="fas fa-users"></i>
                            </div>
                            <div class="feature-cont">
                                <div class="feature-text">Experienced Teachers</div>
                            </div>
                        </div>
                        <p class="mb-0">Ardent MDS has a team of dedicated teachers, experienced in the field of dental education, preparing students for NEET MDS, striving to give the best in simple yet profound manner, since several years.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="feature-box text-center">
                        <div class="feature-header">
                            <div class="feature-icon">
                                <span class="circle"></span><i class="fa fa-building"></i>
                            </div>
                            <div class="feature-cont">
                                <div class="feature-text">Accessible location</div>
                            </div>
                        </div>
                        <p class="mb-0">Ardent MDS has its offline centre at Guindy, Chennai, near the heart of the city, easily accessible to various modes of transportation such as Chennai metro, MTC bus stops and Railway station.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="feature-box text-center">
                        <div class="feature-header">
                            <div class="feature-icon">
                                <span class="circle"></span><i class="fa fa-cogs"></i>
                            </div>
                            <div class="feature-cont">
                                <div class="feature-text">Innovative Methodology</div>
                            </div>
                        </div>
                        <p class="mb-0">Ardent MDS has a superior competence with innovative learning approach from traditional to recent concepts; the offline classes are integrated with online app for comprehensive understanding.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="feature-box text-center">
                        <div class="feature-header">
                            <div class="feature-icon">
                                <span class="circle"></span><i class="fa fa-book"></i>
                            </div>
                            <div class="feature-cont">
                                <div class="feature-text">Doubt clearing session</div>
                            </div>
                        </div>
                        <p class="mb-0">Students can interact with faculties after every class and clarify their doubts promptly. Ardent MDS gives personal guidance and motivation to their students, which is one of our unique features.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="feature-box text-center">
                        <div class="feature-header">
                            <div class="feature-icon">
                                <span class="circle"></span><i class="fa fa-line-chart"></i>
                            </div>
                            <div class="feature-cont">
                                <div class="feature-text">Regular Test & Analysis </div>
                            </div>
                        </div>
                        <p class="mb-0">Students can evaluate their progress in NEET MDS preparation, by attending regular tests in various aspects. Every test is scrutinized and a complete analysis is given, to help the students evaluate their strength and weakness and modify their preparation periodically.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="feature-box text-center">
                        <div class="feature-header">
                            <div class="feature-icon">
                                <span class="circle"></span><i class="fa fa-address-card-o"></i>
                            </div>
                            <div class="feature-cont">
                                <div class="feature-text">Track record</div>
                            </div>
                        </div>
                        <p class="mb-0">A glimpse of the track record provides an idea of the quality of education we provide; our efficiency in getting students prepared for the exams is evident from our students being alumni or pursuing postgraduation in India’s prestigious institutions.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="./js/plugins.js" type="text/javascript"></script>
    <script src="./js/active.js" type="text/javascript"></script>
    <script src="./js/main.js" type="text/javascript"></script>
    <script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
    <script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>