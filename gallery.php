<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Gallery</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Gallery</span></a>
						</li>
						<li class="trail-item trail-end"><span>Gallery</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="event-area pt-50 pb-50">
		<div class="container">
			<div class="popup-gallery pb-25">
				<ul class="gallery-list">
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/01.jpg" title="Event Name"><img src="./images/gallery/01.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/02.jpg" title="Event Name"><img src="./images/gallery/02.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/13.jpg" title="Event Name"><img src="./images/gallery/13.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/04.jpg" title="Event Name"><img src="./images/gallery/04.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/05.jpg" title="Event Name"><img src="./images/gallery/05.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/06.jpg" title="Event Name"><img src="./images/gallery/06.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/07.jpg" title="Event Name"><img src="./images/gallery/07.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/08.jpg" title="Event Name"><img src="./images/gallery/08.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/09.jpg" title="Event Name"><img src="./images/gallery/09.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/10.jpg" title="Event Name"><img src="./images/gallery/10.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/11.jpg" title="Event Name"><img src="./images/gallery/11.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/12.jpg" title="Event Name"><img src="./images/gallery/12.jpg" alt=""></a></li>
                    <li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/25.jpg" title="Event Name"><img src="./images/gallery/25.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/14.jpg" title="Event Name"><img src="./images/gallery/14.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/15.jpg" title="Event Name"><img src="./images/gallery/15.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/16.jpg" title="Event Name"><img src="./images/gallery/16.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/17.jpg" title="Event Name"><img src="./images/gallery/17.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/18.jpg" title="Event Name"><img src="./images/gallery/18.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/19.jpg" title="Event Name"><img src="./images/gallery/19.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/20.jpg" title="Event Name"><img src="./images/gallery/20.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/21.jpg" title="Event Name"><img src="./images/gallery/21.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/22.jpg" title="Event Name"><img src="./images/gallery/22.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/23.jpg" title="Event Name"><img src="./images/gallery/23.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/24.jpg" title="Event Name"><img src="./images/gallery/24.jpg" alt=""></a></li>
                    <li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/26.jpg" title="Event Name"><img src="./images/gallery/26.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/27.jpg" title="Event Name"><img src="./images/gallery/27.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/28.jpg" title="Event Name"><img src="./images/gallery/28.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/29.jpg" title="Event Name"><img src="./images/gallery/29.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/30.jpg" title="Event Name"><img src="./images/gallery/30.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/31.jpg" title="Event Name"><img src="./images/gallery/31.jpg" alt=""></a></li>
					<li class="content-1 img-hover-zoom--brightness"><a href="./images/gallery/32.jpg" title="Event Name"><img src="./images/gallery/32.jpg" alt=""></a></li>
				</ul>
			</div>
			<div class="text-center pt-25">
				<a href="#" class="btn btn-theme effect btn-md" id="loadMore">view more</a>
			</div>
		</div>
	</section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>