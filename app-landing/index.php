<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS - APP Landing Page</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="./css/app.css">
    <link rel="stylesheet" href="./css/navbar.css">
    <link href="../owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="../owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="../css/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="../magnific-popup/magnific-popup.css">
</head>
<body>

    <header class="header-area">
	    <div class="fitness-main-menu">
	    	<div class="classy-nav-container breakpoint-off">
	    		<div class="container">
	    			<nav class="classy-navbar justify-content-between" id="fitnessNav">
	    				<a href="./index.php" class="nav-brand">
                            <img src="../images/logo-transparent.png" class="whitelogo" alt="Ardent Logo">
                            <img src="../images/logo.png" class="logo" alt="Ardent Logo">
                        </a>
	    				<div class="classy-navbar-toggler">
	    					<span class="navbarToggler"><span></span><span></span><span></span></span>
	    				</div>
	    				<div class="classy-menu">
	    					<div class="classycloseIcon">
	    						<div class="cross-wrap">
	    							<span class="top"></span>
	    							<span class="bottom"></span>
	    						</div>
	    					</div>
	    					<div class="classynav">
	    						<ul>
	    							<li><a href="#home" class="smooth-goto item">Home</a></li>
	    							<li><a href="#feature" class="smooth-goto item">Features</a></li>
	    							<li><a href="#screenshot" class="smooth-goto item">Screenshot</a></li>
	    							<!-- <li><a href="#download" class="smooth-goto item">Download</a></li> -->
	    							<!-- <li><a href="#faq" class="smooth-goto item">FAQs</a></li> -->
	    							<li><a href="#contact" class="smooth-goto item">Contact</a></li>
	    						</ul>
	    					</div>
	    				</div>
	    			</nav>
	    		</div>
	    	</div>
	    </div>
	</header>

    <section class="s-home shape-primary section-circle" id="home">
        <div class="shape shape-style-1">
            <span class="circle-150"></span>
            <span class="circle-50"></span>
            <span class="circle-50"></span>
            <span class="circle-75"></span>
            <span class="circle-100"></span>
            <span class="circle-75"></span>
            <span class="circle-50"></span>
            <span class="circle-100"></span>
            <span class="circle-50"></span>
            <span class="circle-100"></span>
        </div>
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="home-content">
						<h1>We make the world a better place</h1>
						<p><span style="line-height: 28px">Sign up or Sign in and become a member today to get exclusive benefits and other freebies.</span></p>
						<a href="https://play.google.com/store/apps/details?id=com.ardentmds" target="_blank" class="app-btn"><i class="fab fa-google-play mr-1"></i>Playstore</a>
					</div>
				</div>
				<div class="col-lg-6 home-banner">
					<img src="./images/background/app-home1.png" alt="">
				</div>
			</div>
		</div>
		<div class="section-shape">
            <img src="./images/background/bg-wave.svg" alt="shape image">
        </div>
	</section>

    <section class="s-service pt-60 pb-50">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="section-title text-center mb-60">
						<p class="paragraph-text">Ardent MDS provides Quality education for pg aspirants preparing for competitive Exams. Learn from India’s Top Faculties. Ardent MDS, India’s one of the rapid emerging learning platform is your companion throughout your journey of exam preparation.</p>
					</div>
				</div>
			</div>
			<div id="service-carousel" class="service-carousel owl-carousel owl-theme owl-loaded owl-drag mt-20">
				<div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/mcq-ofthe-day.png" alt="">
						<small class="item-title">MCQ of the day</small>
					</a>
				</div>
				<div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/3-min.png" alt="">
						<small class="item-title">3 Min Challenge</small>
					</a>
				</div>
				<div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/10-min.png" alt="">
						<small class="item-title">10 min video</small>
					</a>
				</div>
				<div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/clinical.png" alt="">
						<small class="item-title">Clinical Vignettes</small>
					</a>
				</div>
				<div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/trending.png" alt="">
						<small class="item-title">Trending Videos</small>
					</a>
				</div>
                <div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/quote.png" alt="">
						<small class="item-title">Quote of the day</small>
					</a>
				</div>
                <div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/question-bank.png" alt="">
						<small class="item-title">Question Bank</small>
					</a>
				</div>
                <div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/test-series.png" alt="">	
						<small class="item-title">Test Series</small>
					</a>
				</div>
                <div class="item">
					<a href="#" class="service-item">
						<img src="./images/icon/master-videos.png" alt="">
						<small class="item-title">Master videos</small>
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="s-about pt-60 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<img src="../images/background/online-class.png" alt="">
				</div>
				<div class="col-lg-6 column-align-middle">
					<div class="inline-text-inner">
						<h3>Why choose our app for your studying ?</h3>
					</div>
					<p>In Ardent app, you can attend Live classes, Recorded classes by top faculties, get your doubts cleared and evaluate your preparation through various test series & Practice session. Also, it contains question bank for all subjects to evaluate your preparation. This series covers mini tests, Grand Test, subject Test.Mini Test: Several Mini Test will be given to evaluate your preparation and analyse your weak areas.</p>
					<a href="https://play.google.com/store/apps/details?id=com.ardentmds" target="_blank" class="app-btn col1">Download Now</a>
				</div>
			</div>
		</div>
		<div class="shape-divider bottom-divider flip-divider">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 1920 120">
				<polygon fill-rule="evenodd" points="0 0 1920 120 0 120"></polygon>
			</svg>
		</div>
	</section>

	<section class="s-feature pt-50 pb-10" id="feature">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="section-title text-center mb-30">
						<h3>Ardent MDS Features</h3>
						<p class="paragraph-text">We made your preparation simplified by giving concept based videos by experts & experienced facluties. Image based MCQ's, Numerical MCQ's, High yield MCQ's, Detailed explanations of notes for MDS preparation. Also recent and previous years paper included in our app.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-sm-12 column-align-middle">
					<div class="single-feature mb-2">
						<div class="feature-icon">
							<div class="icon icon-shape bg-color white-text">
								<i class="fa fa-object-ungroup"></i>
							</div>
						</div>
						<div class="feature-content">
							<h5>Full analytics</h5>
							<p> Full Analytics helps students to develop their performance in various subjects.</p>
						</div>
					</div>
					<div class="single-feature mb-2">
						<div class="feature-icon">
							<div class="icon icon-shape bg-color white-text">
								<i class="fa fa-diamond"></i>
							</div>
						</div>
						<div class="feature-content">
							<h5>Strong/Weak analysis</h5>
							<p class="mb-0">It is helps students to increase their performance in particular subjects.</p>
						</div>
					</div>
					<div class="single-feature mb-2">
						<div class="feature-icon">
							<div class="icon icon-shape bg-color white-text">
								<i class="fa fa-fire"></i>
							</div>
						</div>
						<div class="feature-content">
							<h5>Time taken</h5>
							<p class="mb-0">Improve accuracy of the student with effective timeliness</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-sm-6">
					<img src="./images/background/feature.png" alt="">
				</div>
				<div class="col-lg-4 col-sm-6 column-align-middle">
					<div class="single-feature mb-2">
						<div class="feature-icon">
							<div class="icon icon-shape bg-color white-text">
								<i class="fa fa-file-text-o"></i>
							</div>
						</div>
						<div class="feature-content">
							<h5>Percentile</h5>
							<p class="mb-0">It shows the average percentile score of exam to competitive with to top ranks</p>
						</div>
					</div>
					<div class="single-feature mb-2">
						<div class="feature-icon">
							<div class="icon icon-shape bg-color white-text">
								<i class="fa fa-tasks"></i>
							</div>
						</div>
						<div class="feature-content">
							<h5>Review</h5>
							<p class="mb-0">Review board where you can review and clarify your answer doubts.</p>
						</div>
					</div>
					<div class="single-feature mb-2">
						<div class="feature-icon">
							<div class="icon icon-shape bg-color white-text">
								<i class="fa fa-comment-o"></i>
							</div>
						</div>
						<div class="feature-content">
							<h5>Bookmark</h5>
							<p class="mb-0">Bookmarks helps you to have a progress of the previous reading sessions.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="s-screenshot pt-60 pb-50" id="screenshot">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div class="section-title text-center mb-20">
						<h3>User interface for the user experience</h3>
						<p class="paragraph-text">It’s a great amalgamation of simple user interface design and exciting features, greatly loved by students, parents, and tutors. Ardent MDS app smooth user interface with no glitches.</p>
					</div>
				</div>
			</div>
			<ul class="screen-list screenshot-slide">
				<li>
					<img src="./images/screen/app-screen1.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen2.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen3.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen4.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen5.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen6.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen7.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen8.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen9.png" alt="app-screen">
				</li>
				<li>
					<img src="./images/screen/app-screen10.png" alt="app-screen">
				</li>
			</ul>
		</div>
	</section>

	<footer class="s-footer pt-50" id="contact">
		<div class="container pb-50">
			<div class="row">
				<div class="col-lg-3 footer-content">
					<a href="./index.php" class="nav-brand"><img src="../images/logo.png" alt=""></a>
					<p>Ardent MDS provides Quality education for pg aspirants preparing for competitive Exams.</p>
					<div class="social-links ul-li clearfix">
						<ul class="clearfix">		
							<li><a href="https://www.facebook.com/karthikeyan.smds"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="https://www.instagram.com/ardent_mds/"><i class="fab fa-instagram"></i></a></li>					
							<li><a href="https://www.youtube.com/channel/UCCrwVi_Xr876gPh7v22nbKw"><i class="fab fa-youtube"></i></a></li>		
						</ul>
					</div>
				</div>
				<div class="col-lg-2">
					<h5>Features</h5>
					<ul class="footer-links">
						<li>
							<a href="#">Customer Support</a>
						</li>
						<li>
							<a href="#">Terms</a>
						</li>
						<li>
							<a href="#">Privacy Policy</a>
						</li>
						<li>
							<a href="#">Refunds</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-4">
					<h5>Contact</h5>
					<ul class="footer-text">
						<li>
							<span class="footer-icon">
								<i class="fa fa-headphones"></i>
							</span>
							<a>+91 98849 60484</a>
						</li>
						<li>
							<span class="footer-icon">
								<i class="fa fa-map-marker"></i>
							</span>
							<a>R.V Towers, Opp to Hotel Hablis, G.S.T Road, Guindy, Chennai - 600032</a>
						</li>
						<li>
							<span class="footer-icon">
								<i class="fa fa-envelope"></i>
							</span>
							<a>info.ardentmds@gmail.com</a>
						</li>
					</ul>
				</div>
				<div class="col-lg-3 icon-1 column-align-middle">
					<a href="https://play.google.com/store/apps/details?id=com.ardentmds" target="_blank"><img src="./images/icon/icon-android.svg" alt=""></a>
				</div>
			</div>
		</div>
		<div class="footer-copyright pt-10">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="copyright text-md-left pt-15">
							<p>© Copyrights 2020 All rights reserved. </p>
						</div>
					</div>
					<div class="col-md-4">
						<div class="copyright text-md-right text-center pt-15">
							<p>Deliberated By <span><a href="https://www.mu3interactive.com/" target="_blank">MU3 WebSolutions</a></span> </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<a href="javascript:" id="be-themes-back-to-top">
		<svg width="14" height="9" viewBox="0 0 14 9" xmlns="http://www.w3.org/2000/svg"><path d="M13 7.37329L7 2.00004L1 7.37329" stroke-width="2" stroke-linecap="round"></path></svg>
	</a>
	

    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="../js/plugins.js" type="text/javascript"></script>
	<script src="../js/active.js" type="text/javascript"></script>
	<script src="../js/main.js" type="text/javascript"></script>
	<script src="../js/slick.min.js" type="text/javascript"></script>
    <script src="../owl-carousel/js/owl.carousel.min.js"></script>
    <script src="../magnific-popup/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function(){
			$('.smooth-goto').on('click', function() {  
				$('html, body').animate({scrollTop: $(this.hash).offset().top - 50}, 2000);
				return false;
			});
		});
		$(".item").on("click", function(e){
			$("a.item").removeClass("active");
			$(this).addClass("active");
		});
	</script>

</body>
</html>