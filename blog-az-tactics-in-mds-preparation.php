<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Blog Details</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Blog</span></a>
						</li>
						<li class="trail-item trail-end"><span>Blog</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="event-area pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<img src="./images/blog/blog-a-z.jpeg" class="mt-3" alt="">
					<div class="overview">
						<div class="section-title pb-10">
							<h5>A to Z tactics in MDS preparation</h5>
                            <p class="content">Are you a PG aspirant??? Never mind whatever you are doing, may it be undergraduation, internship or post-internship, start now! <em>There’s never a wrong time, to do the right thing</em>. All you need is an aspiring mind and a willing heart to achieve your dreams.</p>
							<span style="font-size: 16px;color:#010101;font-weight:600">Here is ‘A to Z tactics in MDS preparation’:</span>
                            <p><span>A – Attitude:</span> You are here reading this, because of the aspiration you already have, <b>the aspiration to get your dream MDS seat</b>. So let us see what ‘A’ we have for you to have.</p>
                            <p><span>A – Aspiration:</span> <em>‘A way of thinking about something’</em>. Think only positive. Well begun is half done. With the right attitude, you are already half way there towards your MDS.</p>
                            <p><span>B – Better:</span> Strive to do better than before. </p>
                            <p><span>C – Competition:</span> Compete with yourself, for you are your best friend and worst enemy. Compare yourself only to who you were yesterday. Be your own competitor and <b>fight for excellence</b>. </p>
                            <p><span>D – Definition:</span> <em>‘Clarity of what you do/learn’.</em> Be clear with what you are supposed to learn; what NEET/INICET demands you to know. (read our blog on this).</p>
                            <p><span>E – Education:</span> <em>Education is what remains after one has forgotten everything he learnt in school.</em> <b>Education is an enlightening experience</b>. Learn, learn in a manner, that your dental curriculum is not just for you to pass your examinations, but for it to be retained and applied throughout your life.</p>
                            <p><span>F – Faith:</span> The trust you keep on yourself should be in such a way that you see your <b>future-self</b> in a position you aspired to be; and stay <b>focused</b>. </p>
                            <p><span>G – Goals:</span> Goals have deadlines. Unless you act on it, it is just dreams. Start from the scratch. Set your goals – daily, weekly and monthly goals, and act on it. </p>
                            <p><span>H – Hard work:</span> <em>‘A great deal of effort or endurance’.</em> You get what you deserve; And you deserve, what you work for. So work, work hard, work harder, until you see yourself achieving your goals.</p>
                            <p><span>I – Intelligence:</span> <em>‘The ability to acquire and apply knowledge and skills’.</em> Nobody is born intelligent; they all have trained their minds to do so. </p>
                            <p><span>J – Journey:</span> Make your journey cherishable. <b>Success or failure is a part of the journey and never the end.</b> </p>
                            <p><span>K – Knowledge:</span> <em>‘Awareness of a fact/situation’.</em> When you see yourself succeeding in the daily goals, that’s good; keeping working in the same way. But when you see failure, do not get dejected. Know where you go wrong, analyze the facts and advance towards your goal. <b>Knowledge controls the access to opportunity and advancement.</b></p>
                            <p><span>L – Love:</span> Love what you do, everything falls in place. </p>
                            <p><span>M - Motivation:</span> Motivation is the process that initiates, guides, and maintains goal-oriented behaviors. Keep your motivation so high, that you reach out to your goals effectively. </p>
                            <p><span>N – Nourishment:</span> <em>‘The nourishment of your bodies and of your minds’ </em> is equally important, after all you have to nourish to flourish. Keep your body, mind and soul healthy.</p>
                            <p><span>O – Opinion:</span> <em>‘A statement of advice by an expert’. </em> If in doubt, get an opinion. Get your doubts cleared promptly. </p>
                            <p><span>P – Persuade and Perceive through Persistence:</span> Nothing comes in a day, it is only those who are persistent and willing to study things deeply, who achieve the masterwork. </p>
                            <p><span>Q – Quality:</span> <em>‘The degree of excellence’. </em> Maintain your standards. Never go low with your quality of work.</p>
                            <p><span>R – Responsible:</span> You are responsible for every result in your life. If you want different results, change your thoughts and actions; but never give up. Remember why you started! </p>
                            <p><span>S – Sensible:</span> <em>Be practical and functional. If one way works well for an individual, it does not necessarily work the same for all. Know your capabilities and devise your way with expert’s help.</em> </p>
                            <p><span>T – Time Management:</span> Time management is important not only during the examination, but also during the preparation. Learn how to manage time efficiently, from the experienced faculty. </p>
                            <p><span>U – Understand:</span> Perceive the intended meaning of what is what, in the very first time you come across something, so that you can interpret in any situation. </p>
                            <p><span>V – Value:</span> Appreciate your efforts, for your efforts are valued definitely, even if not immediately. </p>
                            <p><span>W – Wisdom:</span> Only true wisdom is in knowing, what you know, and what you do not know. Work for what you must know. </p>
                            <p><span>X – NEVER:</span> The man who asks a question is a fool for a minute; the man who does not ask is a fool for life. <b>Never hesitate</b> to ask for what you do not know. What seems little now, may occupy the biggest part later. </p>
                            <p><span>Y – Yearn:</span> <em>‘Have an intense feeling of longing for something’.</em> One does not yearn for something that is easily acquired. MDS does not come easily, yearn, yearn with your heart and soul and you will earn it!</p>
                            <p class="mb-5"><span>Z - Zeal:</span> <em>‘Dedication or enthusiasm for something’.</em> Success is due less to ability than to zeal. Through lack of zeal, knowledge is lost.</p>
                            <p class="content">We at <b style="color: #FF6200;">ARDENT</b> MDS, with enthusiastic learning, have the <b>A</b>spiration and <b>R</b>esponsibility – to get you towards your MDS, with a  <b>D</b>edicated and Determined team, that is <b>E</b>motionally connected with you, so that you <b>N</b>ever give upon your goals and <b>T</b>rust in the journey of MDS preparation.</p>
						</div>
					</div>
				</div>
                <div class="col-lg-4">
                    <div class="course-features">
						<div class="course-info">
							<h2 class="widget-title">Related Posts</h2>
                            <ul class="course-info-list">
                                <li><i class="fa fa-angle-right"></i><a href="./blog-what-neet-mds-demands-you-to-know.php"><span class="label">What NEET MDS demands you to know?</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-pathology-preparation.php"><span class="label">Strategies in Oral Pathology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-dental-anatomy-embryology-and-oral-histology-preparation.php"><span class="label">Strategies in Dental Anatomy, Embryology and Oral Histology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-conservative-dentistry-and-endodontics-preparation.php"><span class="label">Strategies in Conservative Dentistry and Endodontics Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-radiology-preparation.php"><span class="label">Strategies in Oral Radiology Preparation</span></a></li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>