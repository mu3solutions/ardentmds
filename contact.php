<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Contact</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Contact</span></a>
						</li>
						<li class="trail-item trail-end"><span>Contact</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

	<section class="s-contact pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="form-container">
						<div class="section-title pb-10">
							<h5>Contact Us</h5>
						</div>
						<div class="wpforms">
							<form action="#" class="" method="POST">
								<div class="row">
									<div class="col-lg-6 col-sm-12">
										<input type="text" class="form-control" name="contact-name" placeholder="First name" required>
									</div>
									<div class="col-lg-6 col-sm-12">
										<input type="number" class="form-control" name="contact-phone" placeholder="Phone No" required>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<input type="email" class="form-control" name="contact-email" placeholder="Email" required>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<textarea type="text" class="form-control" name="contact-message" placeholder="Message"></textarea>
									</div>
								</div>
								<div class="row">
									<button class="btn btn-theme btn-md effect ml-20">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="form-container space">
						<div class="section-title pb-10">
							<h5>Our Main Office</h5>
						</div>
						<ul class="address-list pb-25">
							<li><i aria-hidden="true" class="fas fa-map-marker-alt"></i><span class="icon-list-text">R.V Towers, Opp to Hotel Habbis, G.S.T Road, Guindy, Chennai - 600032</span></li>
							<li class="mt-3"><i aria-hidden="true" class="far fa-envelope"></i><span class="icon-list-text email">info.ardentmds@gmail.com</span></li>
							<li class="mt-3"><i aria-hidden="true" class="fas fa-phone"></i><span class="icon-list-text">+91 98849 60484</span></li>
						</ul>
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7774.860211030628!2d80.208518!3d13.00826!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3ec41ea2cb8b424d!2sR.V.TOWERS!5e0!3m2!1sen!2sin!4v1596613750428!5m2!1sen!2sin" width="100%" height="240px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="whatsapp">
        <a href="https://wa.me/9884960484" target="_blank"><div class="icon"><i class="fa fa-whatsapp"></i></div></a>
    </div>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>