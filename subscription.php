<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>
    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Subscription Plans</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Subscription</span></a>
						</li>
						<li class="trail-item trail-end"><span>Subscription</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="infra-area pt-50 pb-50">
		<div class="container">
            <div class="row mt-5">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="subscribe-box">
                        <h2 class="subscribe-heading">Ardent Pro Trial Pack</h2>
                        <p class="mt-3">Ideal plan for students to trial the content of the app.Once you are satisfied you can go for a regular plan.</p> 
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/one_month_plan.html" target="_blank" class="sub-link">Know more</a>
                            </div>
                            <div class="col-lg-6">
                                <h4>Price: <span>Rs. 999</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="subscribe-box">
                        <h2 class="subscribe-heading">Ardent MDS INCET May 2021 Pack</h2>
                        <p class="mt-3">Ideal plan for Post Interns includes Videos, Question bank, Test series suitable for INICET May 2021</p> 
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/four_month_plan.html" target="_blank" class="sub-link">Know more</a>
                            </div>
                            <div class="col-lg-6">
                                <h4>Price: <span>Rs. 3999</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="subscribe-box">
                        <h2 class="subscribe-heading">ARDENT NEET MDS 2023 PACK</h2>
                        <p class="mt-3">Ideal plan for final year students /CRRI includes Videos, Question bank, Test series suitable for early preparation and to get extra edge in competitive exams.</p> 
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/two_year_plan.html" target="_blank" class="sub-link">Know more</a>
                            </div>
                            <div class="col-lg-6">
                                <h4>Price: <span>Rs. 15999</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="subscribe-box">
                        <h2 class="subscribe-heading">ARDENT THIRD YEAR STUDENTS PACK</h2>
                        <p class="mt-3">Ideal plan for Third year students includes Videos, Question bank, Test series suitable for early preparation and to get extra edge in competitive exams.</p> 
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/three_year_plan.html" target="_blank" class="sub-link">Know more</a>
                            </div>
                            <div class="col-lg-6">
                                <h4>Price: <span>Rs. 21999</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="subscribe-box">
                        <h2 class="subscribe-heading">ARDENT SECOND YEAR STUDENTS PACK</h2>
                        <p class="mt-3">Ideal plan for second year students includes Videos, Question bank, Test series suitable for early preparation and to get extra edge in competitive exams.</p> 
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/four_year_plan.html" target="_blank" class="sub-link">Know more</a>
                            </div>
                            <div class="col-lg-6">
                                <h4>Price: <span>Rs. 27999</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="subscribe-box">
                        <h2 class="subscribe-heading">ARDENT FIRST YEAR STUDENTS PACK</h2>
                        <p class="mt-3">Ideal plan for First year students includes Videos, Question bank, Test series suitable for early preparation and to get extra edge in competitive exams.</p> 
                        <div class="row">
                            <div class="col-lg-6">
                                <a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/five_year_plan.html" target="_blank" class="sub-link">Know more</a>
                            </div>
                            <div class="col-lg-6">
                                <h4>Price: <span>Rs. 35999</span></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="text-center" style="overflow-X: auto;">
                        <table class="table exam-details-table">
							<thead>
                                <tr class="thead-dark">
                                    <th colspan="4">ARDENT NEET MDS 2022 PACK</th>
                                </tr>
								<tr>
									<th>Plan</th>
									<th style="width: 40%;">Plan Details</th>
									<th>Price</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
                                <tr>
                                    <td><b>Plan A - Test Series</b></td>
                                    <td><p>More than 400+ tests covering each and every chapter of subject.</p></td>
                                    <td><b>Rs. 3999</b></td>
                                    <td><a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/one_year_plan.html" target="_blank" class="sub-link">Know more</a></td>
                                </tr>
                                <tr>
                                    <td><b>Plan B - Question Bank </b></td>
                                    <td><p>22000+ Questions added in Question Bank with recent pattern and relevant explanation.</p></td>
                                    <td><b>Rs. 3999</b></td>
                                    <td><a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/one_year_plan.html" target="_blank" class="sub-link">Know more</a></td>
                                </tr>
                                <tr>
                                    <td><b>Plan C - Video Session</b></td>
                                    <td><p>More than 200 videos of important chapters in subject and chapter wise.</p></td>
                                    <td><b>Rs. 3999</b></td>
                                    <td><a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/one_year_plan.html" target="_blank" class="sub-link">Know more</a></td>
                                </tr>
                                <tr>
                                    <td><b>Plan D - Hot</b></td>
                                    <td><p>More than 400+ Tests,More than 22000+ Questions,More than 200+ Videos.</p></td>
                                    <td><b>Rs. 9999</b></td>
                                    <td><a href="https://app.mu3innovativesolutions.com/ardent-mds-subscription/one_year_plan.html" target="_blank" class="sub-link">Know more</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="./js/plugins.js" type="text/javascript"></script>
    <script src="./js/active.js" type="text/javascript"></script>
    <script src="./js/main.js" type="text/javascript"></script>
    <script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
    <script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>