<header class="header-area">
	<div class="header-top d-none d-lg-block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-5">
					<div class="header-contact">
						<ul>
							<li>
								<i class="fa fa-envelope"></i>
								<a href="mailto:info.ardentmds@gmail.com">info.ardentmds@gmail.com</a>
							</li>
							<li>
								<i class="fa fa-phone"></i><span>+91 98849 60484</span>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="header-class">
						<ul>
							<li>
								Download Our Offcial App:
								<a href="https://play.google.com/store/apps/details?id=com.ardentmds" target="_blank" data-tooltip="Play Store"><i class="fab fa-google-play"></i></a>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="header-right d-flex justify-content-start">
						<div class="social d-flex">
							<span class="follow-us">Follow Us :</span>
							<ul>
								<li><a href="https://www.facebook.com/karthikeyan.smds"data-tooltip="Facebook"><i class="fa fa-facebook-f"></i></a></li>
								<li><a href="https://www.youtube.com/channel/UCCrwVi_Xr876gPh7v22nbKw"data-tooltip="Youtube"><i class="fa fa-youtube"></i></a></li>
								<li><a href="https://www.instagram.com/ardent_mds/"data-tooltip="Instagram"><i class="fa fa-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fitness-main-menu">
		<div class="classy-nav-container breakpoint-off">
			<div class="container">
				<nav class="classy-navbar justify-content-between" id="fitnessNav">
					<a href="index.php" class="nav-brand"><img src="./images/logo.png" alt=""></a>
					<div class="classy-navbar-toggler">
						<span class="navbarToggler"><span></span><span></span><span></span></span>
					</div>
					<div class="classy-menu">
						<div class="classycloseIcon">
							<div class="cross-wrap">
								<span class="top"></span>
								<span class="bottom"></span>
							</div>
						</div>
						<div class="classynav">
							<ul>
								<li><a href="./index.php">Home</a></li>
								<li><a href="#">About Us</a>
									<ul class="dropdown">
										<li><a href="./faculty.php">Faculty</a></li>
										<li><a href="./infrastructure.php">Infrastructure</a></li>
										<li><a href="./gallery.php">Gallery</a></li>
										<!-- <li><a href="event.html">News & Events</a></li> -->
									</ul>
								</li>
								<li><a href="#">Courses</a>
									<ul class="dropdown">
										<li><a href="./offline.php">Offine</a></li>
										<li><a href="./app-landing/index.php" target="_blank">Online</a></li>
									</ul>
								</li>
                                <li><a href="./app-landing/index.php" target="_blank">MDS App</a></li>
                                <li><a href="./subscription.php">Subscription</a></li>
								<li><a href="./contact.php">Contact Us</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
</header>