<footer id="footer-part">
	<div class="footer-top pt-40 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="footer-about mt-40">
						<div class="logo">
							<a href="#"><img src="./images/logo-transparent.png" alt="Logo"></a>
						</div>
						<p>Ardent MDS is an enthusiastic dental platform for enlighting the future of dental students through various competitive exams.</p>
						<ul class="mt-20">
							<li><a href="https://www.facebook.com/karthikeyan.smds"><i class="fa fa-facebook-f"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCCrwVi_Xr876gPh7v22nbKw"><i class="fa fa-youtube"></i></a></li>
							<li><a href="https://www.instagram.com/ardent_mds/"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="footer-link mt-40">
						<div class="footer-title pb-25">
							<h6>Sitemap</h6>
						</div>
						<ul>
							<li><a href="./index.php"><i class="fa fa-angle-right"></i>Home</a></li>
							<li><a href="./infrastructure.php"><i class="fa fa-angle-right"></i>Infrastructure</a></li>
							<li><a href="./offline.php"><i class="fa fa-angle-right"></i>Courses</a></li>
							<li><a href="./app-landing/index.php" target="_blank"><i class="fa fa-angle-right"></i>MDS App</a></li>
						</ul>
						<ul>
							<li><a href="./gallery.php"><i class="fa fa-angle-right"></i>Gallery</a></li>
							<li><a href="./faculty.php"><i class="fa fa-angle-right"></i>Faculty</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>News & Events</a></li>
							<li><a href="./contact.php"><i class="fa fa-angle-right"></i>Contact</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6">
					<div class="footer-link support mt-40">
						<div class="footer-title pb-25">
							<h6>Support</h6>
						</div>
						<ul>
							<li><a href="#"><i class="fa fa-angle-right"></i>FAQS</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>Privacy Policy</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>Conditions</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>Support</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6">
					<div class="footer-address mt-40">
						<div class="footer-title pb-25">
							<h6>Contact Us</h6>
						</div>
						<ul>
							<li>
								<div class="icon">
									<i class="fa fa-home"></i>
								</div>
								<div class="cont">
									<p>R.V Towers, Opp to Hotel Hablis, G.S.T Road, Guindy, Chennai - 600032</p>
								</div>
							</li>
							<li>
								<div class="icon">
									<i class="fa fa-phone"></i>
								</div>
								<div class="cont">
									<p>+91 98849 60484</p>
								</div>
							</li>
							<li>
								<div class="icon">
									<i class="fa fa-envelope-o"></i>
								</div>
								<div class="cont">
									<p>info.ardentmds@gmail.com</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="footer-copyright pt-10">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="copyright text-md-left text-center pt-15">
						<p>&copy; Copyrights 2020 All rights reserved. </p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="copyright text-md-right text-center pt-15">
						<p>Deliberated By <span><a href="https://www.mu3interactive.com/" target="_blank">MU3 WebSolutions</a></span> </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>