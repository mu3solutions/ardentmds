<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ardent MDS</title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="./css/bootstrap.css" rel="stylesheet">
    <link href="./css/style.css" rel="stylesheet">
    <link href="./css/navbar.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
    <link href="./owl-carousel/css/owl.theme.default.min.css" rel="stylesheet">
    <link href="./css/slick.css" rel="stylesheet">
    <link href="./magnific-popup/magnific-popup.css" rel="stylesheet">
</head>
<body>

    <?php include('./include/header.php') ?>

    <section class="page-header" style="background-image: url('./images/background/bg-6.jpg');">
		<div class="container">
			<h2 class="page-title">Blog Details</h2>
			<div class="header-breadcrumb">
				<nav role="navigation" aria-label="Breadcrumbs" class="breadcrumb-trail breadcrumbs">
					<ul class="trail-items">
						<li class="trail-item trail-begin">
							<a href="./index.php" rel="home"><span>Home</span></a>
						</li>
						<li class="trail-item">
							<a href="#"><span>Blog</span></a>
						</li>
						<li class="trail-item trail-end"><span>Blog</span></li>
					</ul>
				</nav>
			</div>
		</div>
	</section>

    <section class="event-area pt-50 pb-50">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<img src="./images/blog/neet-preparation.jpeg" class="mt-3" alt="">
					<div class="overview">
						<div class="section-title pb-10">
							<h5>What NEET MDS demands you to know?</h5>
							<p class="content">NEET stands for National Eligibility – cum – Entrance Test. It tests your eligibility to enter MDS (Master of Dental Surgery) through the knowledge gained during your BDS (Bachelor of Dental Surgery) course.</p>
							<span style="font-size: 16px;color:#010101;font-weight:600">NEET MDS is a 3-hours Computer – Based Test which comprises of 240 questions, with subject-wise distribution as follows:</span>
							<p class="content">All the subjects in the dental curriculum (BDS syllabus) are covered in NEET MDS examinations. Equal importance is given to both basic medical sciences and clinical dental subjects. Preparation for MDS is not a separate entity; it goes hand in hand with your BDS course itself. If you have utilized your time during undergraduation with avid reading, you just have to get yourself adapted to Computer-based test, and extended reading to recent advancements in dentistry. In the other case, where you have not given your best during BDS course, or if you have not read entirely, or have skipped chapters/topics to get an average pass in your BDS examinations, MDS preparation is not a cakewalk. You almost have to start from the scratch. At one go, you will have to cover the portions of 4 years. That is quite an enormous task. Reading from your textbooks is a Herculean task now. Therefore you will have to seek the help of MDS preparatory books, which have compilation of all the 4-years subjects. Ad remember, reading once or twice, is not enough for you to retain all that, so you need repeated revision and definitely expert guidance.</p>
							<div class="row mt-3 mb-3">
								<div class="col-sm-6">
									<img src="./images/blog/blog-neet-pattern.png" alt="">
								</div>
								<div class="col-sm-6">
									<p><span>Titbits for first/second year BDS students:</span></p>
									<p class="content">You have really great time for a decent preparation. Take time to read, understand concepts and apply the basics as you keep reading further. Interpret the concepts by asking yourself why you have to know them. Remember ‘Little drops of water make a mighty ocean’. The little effort you put in now to read carefully and understand the topics, will save a lot later when you have to rush through all the subjects at one go. Read your book completely, atleast once, because you have enough time now to read 4 subjects books; as compared to reading 4 years books at a time, when reading even one book partly is a big task.</p>
								</div>
							</div>
							<p><span>Titbits for third/final year BDS students:</span></p>
							<p class="content">As you are being introduced into the clinicals, observe, recollect, interpret and enjoy exploring the subjects. If you have laid a strong foundation with your basic science subjects, you really have nothing much to worry about. As you are already in the habit of reading vastly, you will not find it hard, to read double the number of subjects as compared to the first 2 years. In case where you have not utilized your first and second year properly, you will have to put in extra effort to catch up with the basics. Better late than never.</p>
							<p><span>Titbits for students in their internship:</span></p>
							<p class="content">You are the most confused group; more vulnerable and easily carried away by anything said by anybody with regard to NEET MDS. Get expert guidance. It is the one year period – most crucial to make up for the loss of the four years time. You get what you deserve; and you deserve what you work for!. If you have worked really hard during the 4 years of BDS, Kudos to you, you have ample time for revision. The practice you want now is getting familiar to solving MCQs (multiple choice questions), Computer-Based tests, Time Management and Recent topics. On the other hand, if you have not yet gone through most of your syllabus, work hard and harder to catch up for all the time lost. Read – understand – revise – repeat.</p>
							<p class="content">We at <b>ARDENT MDS</b>, provide you with</p>
							<ul class="b">
								<li><i class="fa fa-circle"></i>Study materials based on recent trends</li>
								<li><i class="fa fa-circle"></i>Handmade micro motes</li>
								<li><i class="fa fa-circle"></i>Daily mock test</li>
								<li><i class="fa fa-circle"></i>Live classes</li>
								<li><i class="fa fa-circle"></i>Excellent faculties</li>
								<li><i class="fa fa-circle"></i>24/7 doubt clearing sessions</li>
								<li><i class="fa fa-circle"></i>Enthusiastic learning.</li>
							</ul>
							<p>You are responsible for your results; choose us and we will take up the responsibility.</p>
						</div>
                    </div>
                </div>
				<div class="col-lg-4">
                    <div class="course-features">
						<div class="course-info">
							<h2 class="widget-title">Related Posts</h2>
                            <ul class="course-info-list">
                                <li><i class="fa fa-angle-right"></i><a href="./blog-az-tactics-in-mds-preparation.php"><span class="label">A to Z tactics in MDS preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-pathology-preparation.php"><span class="label">Strategies in Oral Pathology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-dental-anatomy-embryology-and-oral-histology-preparation.php"><span class="label">Strategies in Dental Anatomy, Embryology and Oral Histology Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-conservative-dentistry-and-endodontics-preparation.php"><span class="label">Strategies in Conservative Dentistry and Endodontics Preparation</span></a></li>
                                <li><i class="fa fa-angle-right"></i><a href="./blog-strategies-in-oral-radiology-preparation.php"><span class="label">Strategies in Oral Radiology Preparation</span></a></li>
							</ul>
						</div>
					</div>
                </div>
            </div>
        </div>
    </section>

    <?php include('./include/footer.php') ?>

    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="./js/plugins.js" type="text/javascript"></script>
	<script src="./js/active.js" type="text/javascript"></script>
	<script src="./js/main.js" type="text/javascript"></script>
	<script src="./js/slick.min.js" type="text/javascript"></script>
    <script src="./owl-carousel/js/owl.carousel.min.js"></script>
	<script src="./magnific-popup/jquery.magnific-popup.min.js"></script>
</body>
</html>