(function($) {
    'use strict';
    var browserWindow = $(window);
    browserWindow.on('load', function() {
        $('#preloader').fadeOut('slow', function() {
            $(this).remove();
        });
    });
    if ($.fn.classyNav) {
        $('#fitnessNav').classyNav();
    }
    if ($.fn.scrollUp) {
        browserWindow.scrollUp({
            scrollSpeed: 1500,
            scrollText: '<i class="fa fa-angle-up"></i>'
        });
    }
    if ($.fn.sticky) {
        $(".fitness-main-menu").sticky({
            topSpacing: 0
        });
    }
    $('a[href="#"]').click(function($) {
        $.preventDefault()
    });
    if (browserWindow.width() > 767) {
        new WOW().init();
    }
	
	
	//===== home-slider
	
	$('.slide-one-item').owlCarousel({
        center: false,
        items: 1,
        loop: true,
        stagePadding: 0,
        margin: 0,
        autoplay: true,
        autoplayTimeout: 10000,
        pauseOnHover: false,
        dots: true,
        nav: true,
        navText: ['<span class="fas fa-chevron-left">', '<span class="fas fa-chevron-right">'],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                dots: false,
                nav: false,
            },
            1000:{
                dots: false,
                nav: true,
            },
        }
    });
    
    //===== faculty-slider
	
	$('.faculty-slider').owlCarousel({
        center: false,
        items: 4,
        loop: true,
        stagePadding: 0,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 10000,
        pauseOnHover: false,
        dots: true,
        nav: false,
        navText: ['<span class="fas fa-chevron-left">', '<span class="fas fa-chevron-right">'],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                dots: false,
                nav: false,
            },
            600:{
                items:3,
                dots: true,
                nav: false,
            },
            1000:{
                dots: true,
                nav: false,
            },
        }
    });
	
	
	//===== about-slider
	
	$('#student-details').owlCarousel({
        items: 1,
        loop: true,
        margin: 15,
        autoplay: true,
		dots: true,
		autoplaySpeed: 5000,
        animateOut: 'fadeOut'
    });
	
	$('#student-details-1').owlCarousel({
        items: 1,
        loop: true,
        margin: 15,
        autoplay: true,
		nav:false,
		autoplaySpeed: 5000,
        animateOut: 'fadeOut'
    });
	
	
	//===== app-services-slider
    
    $('.service-carousel').owlCarousel({
        items: 5,
        loop: true,
        margin: 15,
        autoplay: true,
        nav:false,
        dots:false,
        autoplayTimeout: 5000,
        animateOut: 'fadeOut',
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:3,
                animateOut: false,
            },
            1000:{
                items:5,
            }
        }
    });
	
	//===== counter
	
	$('.counter').counterUp({
        delay: 10,
        time: 3000
    });
	
	
	
})(jQuery);